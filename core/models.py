from django.db import models


class Files(models.Model):
    filename = models.CharField(max_length=256, unique=True)
    url = models.CharField(max_length=1024)
