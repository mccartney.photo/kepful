from django.db.transaction import atomic

from core.models import Files


class DataIngestion:

    @classmethod
    @atomic
    def parse_file_names(cls, filename):
        print(filename)
        with open(filename, 'r') as file:
            data = file.read()

        for line in data.split('\n'):
            if line.startswith('#'):
                continue
            entry = line.split('\'')
            if len(entry) != 5:
                continue
            Files(filename=entry[1], url=entry[3]).save()
