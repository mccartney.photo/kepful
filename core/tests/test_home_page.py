from unittest import skip

from django.test import TestCase


class HomePageTestCase(TestCase):
    @skip('not implemented')
    def test_status_code(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
