from django.test import TestCase

from core.etl import DataIngestion
from core.models import Files


class ParseBATFiles(TestCase):

    def test_parse_file_name(self):
        DataIngestion.parse_file_names('wget_bat_files/Kepler_Q1_wget.bat')
        file_exists = Files.objects.filter(
            filename='kplr010000009-2009166043257_llc.fits'
        ).exists()
        self.assertTrue(file_exists)


class GetFiles(TestCase):

    def test_get_file(self):
        file = Files(
            filename='kplr010000009-2009166043257_llc.fits',
            url='http://exoplanetarchive.ipac.caltech.edu:80/data/ETSS//'
                'Kepler/005/159/31/kplr010000009-2009166043257_llc.fits',
        )
        etl = DataIngestion(file)
        etl.get_files()
