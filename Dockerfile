FROM python:3-slim
ENV PYTHONUNBUFFERED 1
RUN mkdir /var/www
RUN mkdir /var/www/kepful
WORKDIR /var/www/kepful
ADD requirements.txt /var/www/kepful
RUN pip install -r requirements.txt
EXPOSE 8000
ADD . /var/www/kepful
